﻿namespace Nolte.Deployment
{
    /// <summary>
    /// Provide a toolset for cloning the application at startup for making it possible to update it while running.
    /// </summary>
    public static class OnlineUpdateable
    {
        /// <summary>
        /// The path to the clones exe file name
        /// </summary>
        private static string tempExeName;

        /// <summary>
        /// Gets a value indicating whether the currently running application is a clone or not
        /// </summary>
        public static bool IsClone
        {
            get
            {
                return System.Environment.CommandLine.Contains("/OnlineUpdateableClone");    
            }
        }

        /// <summary>
        /// Gets the current running applications parent directory
        /// </summary>
        private static string CurrentExePath
        {
            get
            {
                string origdir = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                return origdir.Substring(0, origdir.LastIndexOf(System.IO.Path.DirectorySeparatorChar));
            }
        }
    
        /// <summary>
        /// Create a copy of this program installation on a temporary path
        /// </summary>
        public static void CreateClone()
        {
            if (!IsClone)
            {
                string basename = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                basename = basename.Substring(basename.LastIndexOf(System.IO.Path.DirectorySeparatorChar) + 1);
                basename = basename.Substring(0, basename.LastIndexOf('.'));
                int j = 0;
                string tempdir;
                do
                {
                    tempdir = System.IO.Path.Combine(System.IO.Path.GetTempPath(), basename + j.ToString());
                    j++;
                } 
                while (System.IO.Directory.Exists(tempdir));
                System.IO.Directory.CreateDirectory(tempdir);
                string origdir = CurrentExePath;
                foreach (string dirPath in System.IO.Directory.GetDirectories(origdir, "*", System.IO.SearchOption.AllDirectories))
                {
                    string newDirPath = dirPath.Replace(origdir, tempdir);
                    if (!System.IO.Directory.Exists(newDirPath))
                    {
                        System.IO.Directory.CreateDirectory(newDirPath);
                    }
                }

                foreach (string newPath in System.IO.Directory.GetFiles(origdir, "*.*", System.IO.SearchOption.AllDirectories))
                {
                    System.IO.File.Copy(newPath, newPath.Replace(origdir, tempdir), true);
                }

                tempExeName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName.Replace(origdir, tempdir);
            }
        }

        /// <summary>
        /// Run the cloned application
        /// </summary>
        public static void RunClone()
        {
            if (!IsClone)
            {
                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();
                info.Arguments = System.Environment.CommandLine + " " + "/OnlineUpdateableClone";
                info.FileName = tempExeName;
                System.Diagnostics.Process.Start(info);
            }
        }

        /// <summary>
        /// Deletes the currently running application's parent directory after the given time
        /// </summary>
        /// <param name="seconds">Amount of seconds till the directory get deleted</param>
        public static void CleanUp(int seconds = 3)
        {
            if (IsClone)
            {
                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();
                info.Arguments = string.Format("/C choice /C Y /N /D Y /T {0} & rmdir /s /q \"{1}\"", seconds, CurrentExePath);
                info.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                info.CreateNoWindow = true;
                info.FileName = "cmd.exe";
                System.Diagnostics.Process.Start(info);  
            }
        }
    }
}