﻿using System;
using System.Windows.Forms;

namespace OnlineUpdateableApplication
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (Nolte.Deployment.OnlineUpdateable.IsClone)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
                Nolte.Deployment.OnlineUpdateable.CleanUp(2);
            }
            else
            {
                Nolte.Deployment.OnlineUpdateable.CreateClone();
                Nolte.Deployment.OnlineUpdateable.RunClone();
            }
        }
    }
}
