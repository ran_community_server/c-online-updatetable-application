﻿namespace OnlineUpdateableApplication
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        public Form1()
        {
            this.InitializeComponent();
            label1.Text = "Version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
    }
}
